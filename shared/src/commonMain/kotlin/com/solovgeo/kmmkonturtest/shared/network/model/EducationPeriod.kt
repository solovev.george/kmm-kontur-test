package com.solovgeo.kmmkonturtest.shared.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class EducationPeriod(
    @SerialName("end")
    val end: String?,
    @SerialName("start")
    val start: String?
)