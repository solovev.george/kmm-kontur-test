package com.solovgeo.kmmkonturtest.shared.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class ApiContact(
    @SerialName("biography")
    val biography: String?,
    @SerialName("educationPeriod")
    val educationPeriod: EducationPeriod?,
    @SerialName("height")
    val height: Double?,
    @SerialName("id")
    val id: String?,
    @SerialName("name")
    val name: String?,
    @SerialName("phone")
    val phone: String?,
    @SerialName("temperament")
    val temperament: String?
)

