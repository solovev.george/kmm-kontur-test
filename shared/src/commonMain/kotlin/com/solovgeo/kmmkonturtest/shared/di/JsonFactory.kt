package com.solovgeo.kmmkonturtest.shared.di

import kotlinx.serialization.json.Json

class JsonFactory {
    fun create(): Json = Json {
        isLenient = true
        ignoreUnknownKeys = true
        coerceInputValues = true
    }
}