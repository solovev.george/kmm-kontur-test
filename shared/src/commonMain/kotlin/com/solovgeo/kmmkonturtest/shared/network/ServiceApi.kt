package com.solovgeo.kmmkonturtest.shared.network


import com.solovgeo.kmmkonturtest.shared.network.model.ApiContact
import io.ktor.client.HttpClient
import io.ktor.client.request.get


interface ServiceApi {
    suspend fun getContacts(): List<ApiContact>
}

internal class DefaultServiceApi(
    private val httpClient: HttpClient,
    private val endpoint: String
) : ServiceApi {

    override suspend fun getContacts(): List<ApiContact> {
        return httpClient.get("$endpoint/mobile-test-droid/blob/master/json/generated-01.json")
    }
}