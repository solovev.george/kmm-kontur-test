package com.solovgeo.kmmkonturtest.shared.di

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.features.HttpTimeout

import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.http.ContentType
import io.ktor.http.HeadersBuilder
import io.ktor.http.HttpHeaders
import io.ktor.http.auth.HttpAuthHeader
import kotlinx.serialization.json.Json

internal class HttpClientFactory(
    private val json: Json
) {

    fun create(): HttpClient {
        val configBlock: HttpClientConfig<*>.() -> Unit = {
            install(HttpTimeout) {
                requestTimeoutMillis = 30000
                connectTimeoutMillis = 20000
            }
            install(JsonFeature) {
                serializer = KotlinxSerializer(json)
            }
        }
        return HttpClient(configBlock)
    }
}