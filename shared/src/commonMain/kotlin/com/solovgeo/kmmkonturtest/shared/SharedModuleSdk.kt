package com.solovgeo.kmmkonturtest.shared

import com.solovgeo.kmmkonturtest.shared.di.HttpClientFactory
import com.solovgeo.kmmkonturtest.shared.di.JsonFactory
import com.solovgeo.kmmkonturtest.shared.network.DefaultServiceApi
import com.solovgeo.kmmkonturtest.shared.network.ServiceApi

class SharedModuleSdk {
    private val json = JsonFactory().create()
    private val api: ServiceApi = DefaultServiceApi(
        httpClient = HttpClientFactory(json).create(),
        endpoint = "https://github.com/SkbkonturMobile"
    )
}