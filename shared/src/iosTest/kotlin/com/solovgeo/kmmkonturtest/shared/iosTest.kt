package com.solovgeo.kmmkonturtest.shared

import com.solovgeo.kmmkonturtest.shared.network.ServiceApi
import kotlin.test.Test
import kotlin.test.assertTrue

class IosGreetingTest {

    @Test
    fun testExample() {
        assertTrue(ServiceApi().greeting().contains("iOS"), "Check iOS is mentioned")
    }
}