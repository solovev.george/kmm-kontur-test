package com.solovgeo.kmmkonturtest.shared

import com.solovgeo.kmmkonturtest.shared.network.ServiceApi
import org.junit.Assert.assertTrue
import org.junit.Test

class AndroidGreetingTest {

    @Test
    fun testExample() {
        assertTrue("Check Android is mentioned", ServiceApi().greeting().contains("Android"))
    }
}